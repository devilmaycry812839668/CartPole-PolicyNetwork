# CartPole_PolicyNetwork

#### 介绍
强化学习中的策略网络算法。《TensorFlow实战》一书中强化学习部分的策略网络算法，仿真环境为gym的CartPole，本项目是对原书代码进行了部分重构，并加入了些中文注释，同时给出了30次试验的运行结果。





#### 软件环境
python3.7,
tensorflow-gpu==1.14,
gym,
numpy




#### 软件说明
本项目代码为最原始的策略网络算法（强化学习算法的一种），本代码中将一个episode内的所有的state,action,所选action对应的预测概率p ，以及选择action后所获得的累计折扣奖励 作为输入数据，经过神经网络训练得到各参数对应的梯度，以此类推，一个batch里面的所有的episode都如此训练，一个batch可以得到batch_size个所有神经网络对应的梯度，也就是说神经网络中的任一Variable都有batch_size个对应的梯度，对这batch_size个梯度加总求和作为一个batch的数据所训练出来的梯度更新回神经网络参数中，如此一个batch的训练完成。




